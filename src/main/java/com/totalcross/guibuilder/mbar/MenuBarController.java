package com.totalcross.guibuilder.mbar;

import java.util.ArrayList;

import com.totalcross.guibuilder.Guibuilder;
import com.totalcross.guibuilder.ui.UtilUI;
import com.totalcross.guibuilder.ui.mbar.MenuGroup;
import com.totalcross.guibuilder.ui.mbar.MenuGroupsActions;
import com.totalcross.guibuilder.ui.mbar.MenuItemAction;

import totalcross.io.IOException;
import totalcross.sys.Settings;
import totalcross.sys.Vm;
import totalcross.ui.Button;
import totalcross.ui.ColorList;
import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.ListBox;
import totalcross.ui.MainWindow;
import totalcross.ui.MenuBar;
import totalcross.ui.MenuItem;
import totalcross.ui.PushButtonGroup;
import totalcross.ui.TabbedContainer;
import totalcross.ui.dialog.InputBox;
import totalcross.ui.dialog.MessageBox;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.event.KeyEvent;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;
import totalcross.util.ElementNotFoundException;
import totalcross.util.IntHashtable;

public class MenuBarController {

	private MenuItemAction miSave, miRemove, miAsMainWindow, miAsWindow, miAsContainer, miSelectWidth, miSelectHeight;
	private MenuBar mbar;
	private final Guibuilder gui;
	
	public MenuBarController(Guibuilder gui) {
		this.gui = gui;
	}
	
	private MenuGroupsActions getItems() {
		MenuGroup project = new MenuGroup("Project");
		project.add(new MenuItemAction("New", () -> {
			gui.clearProject();
			miRemove.obj.isEnabled = false;
		}));
		
		project.add(new MenuItemAction("Rename", () -> {
			InputBox d = new InputBox("Project Rename", "Enter the new name:", gui.getProjectName());
			d.popup();
			if (d.getPressedButtonIndex() == 0) {
				String projectName = d.getValue();
				gui.setProjectName(projectName);
				miRemove.obj.isEnabled = false; // can't delete project when renamed
			}
		}));
		
		project.add(new MenuItemAction("Load", () -> {
		}));
		
		project.add(miSave = new MenuItemAction("Save", () -> {
		}));
		
		project.add(miRemove = new MenuItemAction("Remove", () -> {
		}));
		
		project.add(MenuItemAction.getItemSeparator());
		
		project.add(new MenuItemAction("Exit", () -> {
		}));
		miRemove.obj.isEnabled = miSave.obj.isEnabled = false;

		MenuGroup code = new MenuGroup("Code");
		code.add(miAsMainWindow = new MenuItemAction(new MenuItem("As MainWindow", true), () -> {
		}));
		code.add(miAsWindow = new MenuItemAction(new MenuItem("As Window", false), () -> {
		}));
		code.add(miAsContainer = new MenuItemAction(new MenuItem("As Container", false), () -> {
		}));
		code.add(MenuItemAction.getItemSeparator());
		code.add(miAsContainer = new MenuItemAction(new MenuItem("Create Java Code"), () -> {
		}));
		
		MenuGroup attributes = new MenuGroup("Attributes");
		attributes.add(new MenuItemAction("Select foreground", () -> {
		}));
		attributes.add(new MenuItemAction("Select background", () -> {
		}));
		attributes.add(MenuItemAction.getItemSeparator());
		attributes.add(miSelectWidth = new MenuItemAction("Select width", () -> {
		}));
		attributes.add(miSelectHeight = new MenuItemAction("Select height", () -> {
		}));
		miSelectWidth.obj.isEnabled = miSelectHeight.obj.isEnabled = false;
		
		MenuGroup help = new MenuGroup("?");
		help.add(new MenuItemAction("Instructions", () -> {
			new MessageBox("Instructions", "To Insert: Set the attributes and\nposition, then click the button with\nthe control you want to add.\nTo Update: select the control you\nwant to change, modify its\nattributes, click '+'\nTo Delete: select the control in the\nlistbox, click '-'. Note: be careful\nwhen deleting controls that are\nused by others as 'Relative to'.\nTo change the controls order,\nclick the arrows (up/down).").popup();
		}));
		help.add(new MenuItemAction("About", () -> {
			new MessageBox("About", "GUIBuilder " + Guibuilder.version + "\nCreated by Yehuda Miron\nEnhanced by Guilherme C. Hazan\nEnhanced by the TotalCross team").popup();
		}));
		
		ArrayList<MenuGroup> groups = new ArrayList<>();
		
		groups.add(project);
		groups.add(code);
		groups.add(attributes);
		groups.add(help);

		return new MenuGroupsActions(groups);
	}
	
	public MenuBar getMenuBar() {
		if (mbar == null) {
			MenuGroupsActions groupsActions = getItems();
			mbar = new MenuBar(groupsActions.getItems());
			
			mbar.addPressListener(UtilUI.getMapPressListener(() -> mbar.getSelectedIndex(), groupsActions.getActionMap()::get));
//			     int sel = mbar.getSelectedIndex();
//			     
//			     Runnable action = actionMap.get(sel);
//			     
//			     if (action != null) {
//			    	 action.run();
//			     }
//			     switch (sel)
//			     {
//			        case -1: break;
//			        case 3: // Load
//			           ComboBox filesCombo = new ComboBox();
//			           persist.fillListNames(filesCombo, false);
//			           String from = "catalogs";
//			           if (filesCombo.size() == 0)
//			              showMsg("Attention","No projects found in "+from);
//			           else
//			           {
//			              // select the last project to make easier for the user
//			              int indx;
//			              if (lastProject != null && (indx=filesCombo.indexOf(lastProject)) != -1)
//			                 filesCombo.setSelectedIndex(indx);
//	//		              ChoicesDialog d = new ChoicesDialog("Loading from "+from,"Select the project name:", filesCombo);
//	//		              d.popupBlockingModal();
//	//		              if (d.getPressedButtonIndex() == 0 && d.getComboBox().getSelectedIndex() != -1)
//	//		              {
//	//		                 if (!readData(false, (String)d.getComboBox().getSelectedItem()))
//	//		                    showMsg("Error","Load from Catalog");
//	//		                 else
//	//		                    miRemove.isEnabled = true; // enable remove of project
//	//		              }
//			           }
//			           break;
//			        case 4: // Save
//			           ok = persist.saveString(projectName, false, prepareData());
//			           showMsg(ok?"Done":"Error", "Save to Catalog");
//			           if (ok)
//			           {
//			              lastProject = projectName;
//			              miRemove.isEnabled = true;
//			           }
//			           break;
//			        case 5: // Use Memo
//			           miRemove.isEnabled = false;
//			           break;
//			        case 6: // Remove
//			           {
//			              MessageBox mb = new MessageBox("Attention","Do you really want|to remove from|"+("Catalog")+"|project "+projectName+'?',new String[]{"Yes","No"});
//			              mb.popup();
//			              if (mb.getPressedButtonIndex() == 0)
//			              {
//			                 if (!persist.remove(projectName, false))
//			                    showMsg("Error","Can't find project");
//			                 else
//			                 {
//			                    showMsg("Success","Project removed");
//			                    miRemove.isEnabled = false; // disable remove
//			                 }
//			              }
//			           }
//			           break;
//			        case 8: // Exit
//			           exit(0);
//			           break;
//			        case 101:
//			        case 102:
//			        case 103:
//			           setProjectType(sel);
//			           break;
//			        case 105: // Create Java code
//			           ok = persist.saveString("", true, prepareJavaAsString());
//			           showMsg(ok?"Done":"Error", "Create java code in "+("console"));
//			           break;
//			        case 201: // Select foreground
//			        {
//			           cbColors.setSelectedIndex(cbColors.indexOf("" + getForeColor()));
//	//		           ChoicesDialog d = new ChoicesDialog("Color chooser","Please select the foreground|color of the MainWindow",cbColors);
//	//		           d.popup();
//	//		           if (d.getPressedButtonIndex() == 0)
//	//		           {
//	//		              int c = Color.getRGB(d.getComboBox().getSelectedItem().toString());
//	//		              setForeColor(c);
//	//		              container.setForeColor(c); // guich@520_12
//	//		              previewButton.setForeColor(c);
//	//		              repaint();
//	//		           }
//			           break;
//			        }
//			        case 202: // Select background
//			        {
//			           cbColors.setSelectedItem("" + getBackColor());
//	//		           ChoicesDialog d = new ChoicesDialog("Color chooser","Please select the background|color of the MainWindow",cbColors);
//	//		           d.popupBlockingModal();
//	//		           if (d.getPressedButtonIndex() == 0)
//	//		           {
//	//		              int c = Color.getRGB(d.getComboBox().getSelectedItem().toString());
//	//		              setBackColor(c);
//	//		              container.setBackColor(c); // guich@520_12
//	//		              previewButton.setBackColor(c);
//	//		              repaint();
//	//		           }
//			           break;
//			        }
//			        case 204: // Select width
//			        {
//			           InputBox d = new InputBox("Width","Enter Window Width",wcWidth+"");
//			           d.getEdit().setMode(Edit.CURRENCY);
//			           d.popup();
//			           if (d.getPressedButtonIndex() == 0)
//			           {
//			              wcWidth = Integer.valueOf(d.getValue());
//			              if (wcWidth == 0)
//			                 wcWidth = getClientRect().width;
//			              container.setRect(LEFT,TOP,wcWidth, wcHeight);
//			              updatePositions();
//			           }
//			           break;
//			        }
//			        case 205: // Select width
//			        {
//			           InputBox d = new InputBox("Height","Enter Window Height",wcHeight+"");
//			           d.getEdit().setMode(Edit.CURRENCY);
//			           d.popup();
//			           if (d.getPressedButtonIndex() == 0)
//			           {
//			              wcHeight = Integer.valueOf(d.getValue());
//			              if (wcHeight == 0)
//			                 wcHeight = getClientRect().height;
//			              container.setRect(LEFT,TOP,wcWidth, wcHeight);
//			              updatePositions();
//			           }
//			           break;
//			        }
//			     }
		}
		
		return mbar;
	}

	public void clearProject() {
		miSave.obj.isEnabled = false;
	}

	public void setDirty() {
		miSave.obj.isEnabled = true;
	}
	
	
}
