package com.totalcross.guibuilder;


import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import com.totalcross.guibuilder.items.Item;
import com.totalcross.guibuilder.mbar.MenuBarController;
import com.totalcross.guibuilder.tabs.Attributes;
import com.totalcross.guibuilder.tabs.Position;
import com.totalcross.guibuilder.ui.pbg.NameAction;
import com.totalcross.guibuilder.ui.pbg.PBGBuilder;

import totalcross.io.IOException;
import totalcross.sys.Settings;
import totalcross.sys.Vm;
import totalcross.ui.Button;
import totalcross.ui.ColorList;
import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import totalcross.ui.Control;
import totalcross.ui.ListBox;
import totalcross.ui.MainWindow;
import totalcross.ui.MenuBar;
import totalcross.ui.PushButtonGroup;
import totalcross.ui.TabbedContainer;
import totalcross.ui.Toast;
import totalcross.ui.dialog.MessageBox;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;
import totalcross.util.concurrent.Lock;

/**
 * main class to handle screen & buttons
 *
 *GUIBuilder:

The application allows the user to design the Graphical User Interface (GUI)
for his/her application. This program runs on the palm itself so the designer 
don't need to carry his PC with him until the application code need to be 
written.<p>

The program supports application with one main window and the following controls:
button,
editbox,
label,
radio Button,
check box,
listbox,
combobox,

<p>usage:
define the control position (x,y), size (sx,sy),name and press
the type of control you want to add.

<p>the "?" button allows the user to see how the screen looks with the defined controls.

<p>selecting a control from the list box allows you to update it properties 
or delete it.

<p>save/load allows to save the design for updating it later on.
the design can be saved either in a catalog or a memo (see the "use memo" checkbox)
the "cat" button allows to see all saved designed (only those saved as catalog).
the combo box allows to select a catalog name to load/save (use the "cat" button
to fill the combo box).

<p>the "->java" button generates a memo with waba template including the design finished.
(sync the memo into outlook, copy it into a text file and it is ready for compilation).


<p>special classes:
persistant: wrap around for all catalog & memo access
debug: pop up messages.

<p>Overall changed by Guich in SuperWaba 3.0

 *
 * @author Yehuda Miron
 * @version 1, 9   modify to suit superwaba v2 beta 3
 * 
 */
public class Guibuilder extends MainWindow {
	public static final String version = "3.0.0";

	List<Item> items = new ArrayList<>();
	int itemCount = 0;
	boolean previewing;
	Persistant persist = new Persistant("GUIB");
	String projectName = "Proj1";
	String lastProject;
	int wcWidth;
	int wcHeight;

	ListBox itemsList;
	Button previewButton;
	Container contents;
	Container controls;
	Container previewContainer;
	PushButtonGroup pbgControls;
	PushButtonGroup pbgActions;
	MenuBar mbar;
	ComboBox cbColors;
	TabbedContainer tp;
	Attributes attrTab = new Attributes();
	Position posTab = new Position();
	MenuBarController mbarController;
	
	public List<Item> getItems() {
		return items;
	}

	public Guibuilder() {
		super("GUIBuilder", TAB_ONLY_BORDER);
	}
	
	private void createAndAddPreviewContainer() {
		contents.add(previewContainer = new Container(), LEFT, TOP, FILL, FILL);
	}

	@Override
	public void initUI() {
		contents = new Container();
		updateTitle();
		
		mbarController = new MenuBarController(this);
		setMenuBar(mbarController.getMenuBar());

		// parse app settings
		if (Settings.appSettings != null && Settings.appSettings.length() > 0) {
			if (Settings.appSettings.length() > 1) {
				lastProject = Settings.appSettings.substring(1);
			}
		}
		
		try {
			previewButton = new Button(new Image("search.png").getSmoothScaledInstance((fmH *3)/2, (fmH *3)/2), Button.BORDER_NONE);
		} catch (ImageException | IOException e) {
			previewButton = new Button("*");
		}
		previewButton.setBackColor(Color.BLACK);
		add(previewButton, LEFT, BOTTOM, FILL, 200 + FONTSIZE);
		add(contents, LEFT, TOP, FILL, FILL -  previewButton.getHeight());
		createAndAddPreviewContainer();
		contents.add(controls = new Container(), LEFT, TOP, FILL, FILL);
		
		previewContainer.setVisible(false);
      
		previewButton.addPressListener((e) -> {
			previewSwap();
		});

		// Edit panel
		PBGBuilder controlsBuilder = new PBGBuilder();

		BiFunction<String, String, NameAction> getNameAction = (caption, name) -> new NameAction(caption, () -> {
			addItemIfValid(name);
		});
    	
		controlsBuilder.add(getNameAction.apply("Btn", "Button"));
		controlsBuilder.add(getNameAction.apply("Edt", "Edit"));
		controlsBuilder.add(getNameAction.apply("Lab", "Label"));
		controlsBuilder.add(getNameAction.apply("Rad", "Radio"));
		controlsBuilder.add(getNameAction.apply("Chk", "Check"));
		controlsBuilder.add(getNameAction.apply("Lbx", "ListBox"));
		controlsBuilder.add(getNameAction.apply("Cbx", "ComboBox"));
		controlsBuilder.addNull();
		controlsBuilder.addNull();
		
		PBGBuilder actionBuilder = new PBGBuilder();
		actionBuilder.add(new NameAction(" ^ "));
		actionBuilder.add(new NameAction(" v "));
		actionBuilder.add(new NameAction(" + "));
		actionBuilder.add(new NameAction(" - ", this::removeSelectedItem));
		
		controls.add(pbgControls = controlsBuilder.build(false, -1, 4, 4, 3, true, PushButtonGroup.BUTTON), RIGHT, TOP + 1, PREFERRED, PREFERRED + 6);
		controls.add(pbgActions = actionBuilder.build(false, -1, 1, 4, 4, true, PushButtonGroup.BUTTON), BEFORE - 2, TOP);
		controls.add(itemsList = new ListBox(), LEFT, SAME, FIT, SAME); // make the listbox the same height of the pbgControls

		controls.add(tp = new TabbedContainer(new String[] { "Attributes", "Position" }), LEFT, AFTER + 2, FILL, FILL);

		tp.getContainer(0).add(attrTab, LEFT, TOP, FILL, FILL);
		tp.getContainer(1).add(posTab, LEFT, TOP, FILL, FILL);

		cbColors = new ComboBox(new ColorList());
	}

	private void removeSelectedItem() {
		int selectedIndex = itemsList.getSelectedIndex();
		if (selectedIndex < 0) {
			showMsg("Title", "Select an element to remove");
		} else {
			// remove it
			Item i = items.remove(selectedIndex);
			itemsList.remove(i.getTitle());
			Item.htItems.remove(i.getTitle());
			itemsList.remove(i.getTitle());
			posTab.resetRelatives(items);
			
			// update count
			itemCount--;

			// select previous item
			selectedIndex = Math.max(0, selectedIndex - 1);
			if (itemCount > 0) {
				itemsList.setSelectedIndex(selectedIndex);
//				showItemFields(items.get(selectedIndex));
			}

			// verify if some control now has an invalid state
			StringBuffer invalids = new StringBuffer(100);
			for (Item anotherItem: items) {
				String relTo = anotherItem.relativeTo();
				if (relTo.length() > 0 && Item.htItems.get(relTo) == null) {
					invalids.append(anotherItem.getTitle());
				}
			}
			if (invalids.length() > 0) {
				showMsg("Attention", "The following controls now have|an invalid 'relative to' reference:|" + invalids);
			}
		}
	}

	private void previewSwap() {
		previewing = !previewing;
		
		if (previewing) {
			populatePreview();
		}
		previewContainer.setVisible(previewing);
		controls.setVisible(!previewing);
		updateTitle();
	}

	private void populatePreview() {
		previewContainer.removeAll();
		createAndAddPreviewContainer();
		
		Item.resetControls();
		
		for (Item i: items) {
			Control c = i.getControlInitialized();
			previewContainer.add(c);
			i.updatePosition(c);
		}
	}

	@Override
	public void onExit() {
		// store the app settings
		Settings.appSettings = ('0') + (lastProject != null ? lastProject : "");
	}

	private void updateTitle() {
		if (previewing) {
			setTitle(projectName);
		} else {
			String s = projectName;
			if (s.length() > 10)
				s = s.substring(0, 9) + "...";
			setTitle("GUIBuilder - " + s);
		}
	}

//   public void onEvent(Event event)
//   {
//      boolean ok;
//
//      switch (event.type)
//      {
//         case KeyEvent.KEY_PRESS:
////            if (((KeyEvent)event).key == IKeys.LAUNCH)
//               popupMenuBar(); // make sure the user saw the menu
//            break;
//         case ControlEvent.FOCUS_OUT:
//            if ((event.target == xEdit || event.target == yEdit || event.target == sxEdit || event.target == syEdit)
//               && ((Edit)event.target).getLength() == 0)
//               ((Edit)event.target).setText("0");
//            break;
//         case ControlEvent.PRESSED:
//            if (event.target == itemsList)
//            {
//               showItemFields(items[itemsList.getSelectedIndex()]);
//            }
//            else
//            if (event.target == pbgControls || event.target == pbgActions)
//            {
//               int selected = ((PushButtonGroup)event.target).getSelectedIndex();
//               if (selected == -1)
//                  break;
//               int xx=0,yy=0,sx=0,sy=0;
//               String name="",text="",fore="",back="";
//               String relTo="";
//               if (event.target == pbgControls || (selected > 1)) // moving up/down does not require those values
//               {
//                  text = textEdit.getText();
//                  if (text.length() == 0 &&
//                     ((event.target == pbgControls && (selected != 1 && selected != 5 && selected != 6)) // edit, combo and listbox can be empty
//                   ||(event.target == pbgActions  && (selected != 2 && selected != 3))))
//                  {
//                     MessageBox mb = new MessageBox("Attention","The text field cannot be blank.|It is used by all controls to|determine its preferred size,|as described below:|. Button: it is the button's text|. Edit: defines the mask, which|  in turn is used to compute|  the preferred width|. Label: is the label's text|. Radio: is the radio's text|. Check: is the check's text|. ListBox and ComboBox: insert|  the items separated by|  comma (,); the largest item|  will define the preferred width");
//                     mb.setTextAlignment(LEFT);
//                     mb.setBackForeColors(0x006600, 0x66FF99);
//                     ((PushButtonGroup)event.target).setSelectedIndex(-1);
//                     mb.popup();
//                     break;
//                  }
//                  // get the real position value based in the selections
//                  try
//                  {
//                  xx = htConst2Val.get(xCombo.getSelectedItem().hashCode()) + Integer.valueOf(xEdit.getText());
//                  yy = htConst2Val.get(yCombo.getSelectedItem().hashCode()) + Integer.valueOf(yEdit.getText());
//                  sx = htConst2Val.get(sxCombo.getSelectedItem().hashCode()) + Integer.valueOf(sxEdit.getText());
//                  sy = htConst2Val.get(syCombo.getSelectedItem().hashCode()) + Integer.valueOf(syEdit.getText());
//                  } catch (ElementNotFoundException e) {}
//                  name = nameEdit.getText();
////                  fore = Settings.isColor?foreCombo.getSelectedItem().toString():Convert.unsigned2hex(waba.ui.UIColors.controlsFore.getRGB(),6);
////                  back = Settings.isColor?backCombo.getSelectedItem().toString():Convert.unsigned2hex(waba.ui.UIColors.controlsBack.getRGB(),6);
//                  relTo = relCombo.getSelectedIndex() > 0 ? relCombo.getSelectedItem().toString() : "";
//               }
//
//               if (event.target == pbgControls)
//               {
//                  switch (selected)
//                  {
//                     case 0:
//                     case 1:
//                     case 2:
//                     case 3:
//                     case 4:
//                     case 5:
//                     case 6: // create new item
//                        addItem(false,classes[selected], name,text,fore,back,xx,yy,sx,sy,relTo);
//                        break;
//                  }
//               }
//               else
//               {
//                  if (itemCount <= 0)
//                     break;
//                  int index = itemsList.getSelectedIndex();
//
//                  switch (selected)
//                  {
//                     case 0: // move item up
//                        if (index > 0)
//                        {
//                           // swap listbox
//                           Object here   = itemsList.getItemAt(index);
//                           Object before = itemsList.getItemAt(index-1);
//                           itemsList.setItemAt(index,before);
//                           itemsList.setItemAt(index-1,here);
//                           itemsList.setSelectedIndex(index-1);
//                           // swap array
//                           Item temp = items[index];
//                           items[index] = items[index-1];
//                           items[index-1] = temp;
//                           updatePositions();
//                        }
//                        break;
//                     case 1:
//                        if (index < itemCount-1)
//                        {
//                           Object here = itemsList.getItemAt(index);
//                           Object next = itemsList.getItemAt(index+1);
//                           itemsList.setItemAt(index,next);
//                           itemsList.setItemAt(index+1,here);
//                           itemsList.setSelectedIndex(index+1);
//                           // swap array
//                           Item temp = items[index];
//                           items[index] = items[index+1];
//                           items[index+1] = temp;
//                           updatePositions();
//                        }
//                        break;
//                     case 2: // update current selected item with the current values
//                     {
//                        String oldTitle = items[index].getTitle();
//                        items[index].update(name,text,fore,back,xx,yy,sx,sy,relTo, this);
//                        updatePositions();
//                        String newTitle = items[index].getTitle();
//                        if (!oldTitle.equals(newTitle))
//                        {
//                           itemsList.setItemAt(index, newTitle);
//                           String sel = relCombo.getSelectedItem().toString();
//                           relCombo.remove(oldTitle);
//                           relCombo.add(newTitle);
//                           relCombo.qsort();
//                           int indx = relCombo.indexOf(sel);
//                           relCombo.setSelectedIndex(indx < 0 ? 0 : indx);
//                        }
//                        if (relCombo.getSelectedItem().equals(newTitle))
//                        {
//                           relCombo.setSelectedIndex(0);
//                           items[index].setRelativeTo(null);
//                           showMsg("Attention","The control cannot be positioned|relative to itself!|Resetting to default...");
//                        }
//                        break;
//                     }
//                     case 3: // delete current selected item
//                     {
//                        // remove it
//                        String title = items[index].getTitle();
//                        itemsList.remove(title);
//                        relCombo.remove(title);
//                        Item.htItems.remove(title);
//                        relCombo.setSelectedIndex(0);
//
//                        // update count
//                        itemCount--;
//                        // delete from Array
//                        for (int i = index; i < itemCount; i++)
//                           items[i] = items[i + 1];
//                        items[itemCount] = null; // set to null so the last element, if removed, can be GC'd
//
//                        // select previous item
//                        index = Math.max(0,index-1);
//                        if (itemCount > 0)
//                        {
//                           itemsList.setSelectedIndex(index);
//                           updatePositions();
//                           showItemFields(items[index]);
//                        } else yCombo.setSelectedIndex(0);
//
//                        // verify if some control now has an invalid state
//                        StringBuffer invalids = new StringBuffer(100);
//                        for (int i = 0; i < itemCount; i++)
//                        {
//                           relTo = items[i].relativeTo();
//                           if (relTo.length() > 0 && !Item.htItems.exists(relTo))
//                              invalids.append(items[i].getTitle());
//                        }
//                        if (invalids.length() > 0)
//                           new MessageBox("Attention","The following controls now have|an invalid 'relative to' reference:|"+invalids).popup();
//                        break;
//                     }
//                  }
//               }
//            }
//      }
//   }
   
	private void setProjectType(int sel) {
//      boolean wasWin = miAsWindow.isChecked;
//      boolean isWin  = sel == 102;
//      miAsMainWindow.isChecked = sel == 101;
//      miAsWindow.isChecked     = sel == 102;
//      miAsContainer.isChecked  = sel == 103;
//      // width and height only available in Window
//      miSelectHeight.isEnabled = miSelectWidth.isEnabled = isWin;
//      if (wasWin && !isWin)
//      {
//         wcWidth = 0;
//         wcHeight = 0;
//      }
//      if (wcWidth == 0)
//         wcWidth = getClientRect().width;
//      if (wcHeight == 0)
//         wcHeight = getClientRect().height;
//      container.setRect(LEFT,TOP,wcWidth,wcHeight);
//      container.setBorderStyle(isWin ? BORDER_SIMPLE : BORDER_NONE);
//      updatePositions();
	}
	
	private void addItemIfValid(String type) {
		ItemAttributes itemAttr = populateFromScreen(new ItemAttributes().confType(type));
		
		List<Validation> validations = validate(itemAttr, new ArrayList<>());
		if (Validation.hasNotes(validations)) {
			showMsg("Error", validations.get(0).note); 
		} else {
			addItem(false, itemAttr);
		}
	}
	
	private boolean nullOrEmpty(String str) {
		return str == null || str.isEmpty();
	}
	
	private List<Validation> validate(ItemAttributes itemAttr, List<Validation> validations) {
		if (nullOrEmpty(itemAttr.getName())) {
			validations.add(new Validation("Name your " + itemAttr.getType() + " object", true));
		} else {
			if (Item.htItems.get(itemAttr.getTitle()) != null) {
				validations.add(new Validation("There already is an object " + itemAttr.getType() + " named "  + itemAttr.getName(), true));
			}
		}
		
		if (nullOrEmpty(itemAttr.getText())) {
			validations.add(new Validation("Set a text for the " + itemAttr.getType() + " named "  + itemAttr.getName(), true));
		}
		return validations;
	}

	private ItemAttributes populateFromScreen(ItemAttributes itemAttr) {
		attrTab.populateFromScreen(itemAttr);
		posTab.populateFromScreen(itemAttr);

		return itemAttr;
	}

	private void addItem(boolean loadingFile, ItemAttributes itemAttr) {
		String type = itemAttr.getType();
		String name = itemAttr.getName();
		String text = itemAttr.getText();
		String fore = itemAttr.getFore();
		String back = itemAttr.getBack();
		int x = itemAttr.getX();
		int y = itemAttr.getY();
		int sx = itemAttr.getSx();
		int sy = itemAttr.getSy();
		String relTo = itemAttr.getRelTo();
		Item item = instantiateItem(type);
		
		if (item != null) {
			item.update(name, text, fore, back, x, y, sx, sy, relTo);
			String title = item.getTitle();
			// same title?
			if (itemsList.indexOf(title) != -1) {
				showMsg("Error", "A control with the same name|already exists. You must choose a new name.");
			} else {
	            // insert to the hashtable that maps the title to the control
	            Item.htItems.put(title, item);
	            // add to the listbox of items and select it
	            itemsList.add(title);
	            itemsList.setSelectedIndex(itemCount);
            // add to the combobox of relativeTo
//            relCombo.add(title);
//            if (!loadingFile)
//            {
//               relCombo.qsort();
//               relCombo.setSelectedIndex(0);
//            }
	            items.add(item);
	            posTab.resetRelatives(items);
	            itemCount++;
	            mbarController.setDirty();
         }
      }
   }

	private Item instantiateItem(String type) {
		try {
			Class<Item> classType = Item.getSubclass(type);
			return classType.newInstance(); // use the default constructor
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			e1.printStackTrace();
			showMsg("Invalid Item", e1.getMessage());
			return null;
		}
	}
   
//   private void showItemFields(Item item)
//   {
//      String []rels;
//
//      // x
//      rels = item.getRelativeX();
//      xEdit.setText(rels[1]);
//      xCombo.setSelectedIndex(xCombo.indexOf(rels[0]));
//      // y
//      rels = item.getRelativeY();
//      yEdit.setText(rels[1]);
//      yCombo.setSelectedIndex(yCombo.indexOf(rels[0]));
//      // sx
//      rels = item.getRelativeSX();
//      sxEdit.setText(rels[1]);
//      sxCombo.setSelectedIndex(sxCombo.indexOf(rels[0]));
//      // sy
//      rels = item.getRelativeSY();
//      syEdit.setText(rels[1]);
//      syCombo.setSelectedIndex(syCombo.indexOf(rels[0]));
//
////      nameEdit.setText(item.getName());
////      textEdit.setText(item.getText());
////      foreCombo.setSelectedIndex(foreCombo.indexOf(Color.getRGB(item.getFore())));
////      backCombo.setSelectedIndex(backCombo.indexOf(Color.getRGB(item.getBack())));
//      relCombo.setSelectedIndex(item.relativeTo().length()==0 ? 0 : relCombo.indexOf(item.relativeTo()));
//   }
   
//   private void setSelected(String title)
//   {
//      for (int i = 0; i < itemCount; i++)
//         if ((items[i].getTitle()).equals(title))
//         {
//            showItemFields(items[i]);
//            itemsList.setSelectedIndex(i);
//            break;
//         }
//   }
   
	public void clearProject() {
		clearItems();
		attrTab.clearProject();
		posTab.clearProject();
		mbarController.clearProject();
	}
	
	private void clearItems() {
		previewContainer.removeAll();
		itemCount = 0;
		items.clear();
		itemsList.removeAll();
	}
   
//   private boolean readData(boolean useMemo, String projectName)
//   {
//      int    i;
//      String name="", text="", type="", relTo="", fore="", back="";
//      String x="", y="", sx="", sy="";
//      String s, title;
//      int projType;
//
//      if (useMemo)
//         title = Persistant.MEMO_TITLE + projectName;
//      else
//         title = projectName;
//
//      if (persist.findText(title, useMemo))
//      {
//         String ver = persist.getTextLine();
//         if (!version.equals(ver))
//         {
//            showMsg("Sorry!","You can't load an|old project with|this version.");
//            return false;
//         }
//         i = 0;
//         clearProject();
//         // set the project type accordingly
//         projType = Integer.valueOf(persist.getTextLine());
//         if (projType == 1) // Window?
//         {
//            wcWidth = Integer.valueOf(persist.getTextLine());
//            wcHeight = Integer.valueOf(persist.getTextLine());
//         }
//         setProjectType(101+projType);
//
//         fore = persist.getTextLine();
//         back = persist.getTextLine();
//         if (fore != null && back != null)
//         {
//            setBackForeColors(Color.getRGB(back), Color.getRGB(fore));
//            previewButton.setBackForeColors(backColor, foreColor);
//            container.setBackForeColors(backColor, foreColor);
//            while (null != (s = persist.getTextLine()))
//            {
//               switch (i)
//               {
//                  case 0: type  = s; break;
//                  case 1: name  = s; break;
//                  case 2: text  = s; break;
//                  case 3: fore  = s; break;
//                  case 4: back  = s; break;
//                  case 5: x     = s; break;
//                  case 6: y     = s; break;
//                  case 7: sx    = s; break;
//                  case 8: sy    = s; break;
//                  case 9: relTo = s;
//                        // create the item
//                        addItem(true,type, name, text, fore, back, Integer.valueOf(x),Integer.valueOf(y),Integer.valueOf(sx),Integer.valueOf(sy), relTo);
//                        i = -1;
//                        break;
//               }
//               i++;
//            }
//         }
//
//         if (itemCount > 0)
//         {
//            relCombo.qsort();
//            relCombo.setSelectedIndex(0);
//            setSelected(items[0].getTitle());
//            this.projectName = projectName;
//            this.lastProject = projectName;
//            updateTitle();
//         }
//         else showMsg("Error","Unable to restore|"+projectName+".|No controls found inside it.");
//         return true;
//      }
//      return false;
//   }
	
	private interface SafeCloseable extends AutoCloseable {
		void close();
	}
	
	private Thread restoreToastThread;
	private Lock lock = new Lock();
	private boolean runningThreadToastRestore = false;

	private SafeCloseable changeToastPosition() {
		synchronized (lock) {
			if (restoreToastThread == null) {
				int toastOriginalY = Toast.posY;
				Toast.posY = PARENTSIZE - 2;
				restoreToastThread = new Thread(null, () -> {
					Vm.safeSleep(1000);
					runOnMainThread(() -> {
						synchronized (lock) {
							Toast.posY = toastOriginalY;
							restoreToastThread = null;
							runningThreadToastRestore = false;
						}
					});
				}, "Restore toast thread");
				return this::restoreToastPosition;
			} else {
				return () -> {};
			}
		}
		
	}
	
	private void restoreToastPosition() {
		synchronized (lock) {
			if (!runningThreadToastRestore) {
				runningThreadToastRestore = true;
				restoreToastThread.start();
			}
		}
	}
	
	private void showMsg(String title, String body) {
		try (SafeCloseable ac = changeToastPosition()) {
			Toast.show(body, 1000, this);
		}
		
	}
//   private String prepareJavaAsString()
//   {
////      if (miAsMainWindow.isChecked)
////         return prepareJavaAsMainWindow();
////      if (miAsWindow.isChecked)
////         return prepareJavaAsWindow();
//      return prepareJavaAsContainer();
//   }

//	private String prepareJavaAsMainWindow()
//   {
//      int    i;
//      StringBuffer sb = new StringBuffer(2048); // StringBuffer in this case is far more efficient. (code size dropped 500 bytes)
//
//      // imports
//      sb.append("// ").append(projectName).append(".java\n\n");
//
//      sb.append("import waba.ui.*;\n");
//      sb.append("import waba.fx.*;\n");
//      sb.append("import waba.sys.*;\n\n");
//
//      // class declaration
//      sb.append("/**\n");
//      sb.append(" * Class declaration\n");
//      sb.append(" *\n");
//      sb.append(" * @author\n");
//      sb.append(" */\n\n");
//
//      sb.append("public class ").append(projectName).append(" extends MainWindow\n{\n");
//      sb.append("   // declarations\n");
//
//      // variables
//      for (i = 0; i < itemCount; i++)
//         sb.append(items[i].getJavaDeclaration());
//
//      // constructor
//      sb.append("\n\n   // init\n   public ").append(projectName).append("()\n   {\n");
//      sb.append("      super(\"").append(projectName).append("\", TAB_ONLY_BORDER);\n   }\n");
//
//      // onStart
//      sb.append("\n   public void onStart()\n   {\n");
//      sb.append("      if (Settings.isColor)\n");
//      sb.append("         setBackForeColors(Color.getColor(0x").append(getBackColor()).append("), Color.getColor(0x").append(getForeColor()).append("));\n\n");
//
//      for (i = 0; i < itemCount; i++)
//         sb.append(items[i].getJavaInit());
//
//      sb.append("\n      if (Settings.isColor)\n      {\n");
//
//      for (i = 0; i < itemCount; i++)
//         sb.append("         ").append(items[i].getTitle()).append(".setBackForeColors(Color.getColor(0x").append(items[i].getBack()).append("), Color.getColor(0x").append(items[i].getFore()).append("));\n");
//
//      sb.append("\n      }\n   }\n\n");
//
//      // onEvent
//      sb.append("   public void onEvent(Event event)\n");
//      sb.append("   {\n");
//      sb.append("      switch (event.type)\n");
//      sb.append("      {\n");
//      //   PRESSED
//      sb.append("         case ControlEvent.PRESSED:\n");
//
//      int added = 0;
//      for (i = 0; i < itemCount; i++)
//      {
//         String strEvent = items[i].getJavaEventPRESSED();
//         if (strEvent.length() > 0)
//         {
//            if (added > 0)
//               sb.append("            else\n");
//            sb.append(strEvent);
//            added++;
//         }
//      }
//
//      sb.append("            break;\n");
//      //   FOCUS_OUT
//      sb.append("\n         case ControlEvent.FOCUS_OUT:\n");
//
//      added = 0;
//      for (i = 0; i < itemCount; i++)
//      {
//         String strEvent = items[i].getJavaEventFOCUS_OUT();
//         if (strEvent.length() > 0)
//         {
//            if (added > 0)
//               sb.append("            else\n");
//            sb.append(strEvent);
//            added++;
//         }
//      }
//
//      sb.append("            break;\n");
//      //   WINDOW_CLOSED
//      sb.append("\n         case ControlEvent.WINDOW_CLOSED:\n");
//      sb.append("            break;\n");
//      sb.append("      }\n");
//      sb.append("   }\n");
//
//      // FINISH
//      sb.append("\n}\n");
//
//      return sb.toString();
//   }
	
//   private String prepareJavaAsWindow()
//   {
//      int    i;
//      StringBuffer sb = new StringBuffer(2048); // StringBuffer in this case is far more efficient. (code size dropped 500 bytes)
//
//      // imports
//      sb.append("// ").append(projectName).append(".java\n\n");
//
//      sb.append("import waba.ui.*;\n");
//      sb.append("import waba.fx.*;\n");
//      sb.append("import waba.sys.*;\n\n");
//
//      // class declaration
//      sb.append("/**\n");
//      sb.append(" * Class declaration\n");
//      sb.append(" *\n");
//      sb.append(" * @author\n");
//      sb.append(" */\n\n");
//
//      sb.append("public class ").append(projectName).append(" extends Window\n{\n");
//      sb.append("   // declarations\n");
//
//      // variables
//      for (i = 0; i < itemCount; i++)
//         sb.append(items[i].getJavaDeclaration());
//
//      // constructor
//      sb.append("\n\n   // init\n   public ").append(projectName).append("()\n   {\n");
//      sb.append("      super(\"").append(projectName).append("\", RECT_BORDER);\n");
//      sb.append("      setRect(CENTER,CENTER,").append(wcWidth).append(',').append(wcHeight).append(");\n");
//      sb.append("      if (Settings.isColor)\n");
//      sb.append("         setBackForeColors(Color.getColor(0x").append(getBackColor()).append("), Color.getColor(0x").append(getForeColor()).append("));\n\n");
//
//      for (i = 0; i < itemCount; i++)
//         sb.append(items[i].getJavaInit());
//
//      sb.append("\n      if (Settings.isColor)\n      {\n");
//
//      for (i = 0; i < itemCount; i++)
//         sb.append("         ").append(items[i].getTitle()).append(".setBackForeColors(Color.getColor(0x").append(items[i].getBack()).append("), Color.getColor(0x").append(items[i].getFore()).append("));\n");
//
//      sb.append("\n      }\n   }\n\n");
//
//      // onEvent
//      sb.append("   public void onEvent(Event event)\n");
//      sb.append("   {\n");
//      sb.append("      switch (event.type)\n");
//      sb.append("      {\n");
//      //   PRESSED
//      sb.append("         case ControlEvent.PRESSED:\n");
//
//      int added = 0;
//      for (i = 0; i < itemCount; i++)
//      {
//         String strEvent = items[i].getJavaEventPRESSED();
//         if (strEvent.length() > 0)
//         {
//            if (added > 0)
//               sb.append("            else\n");
//            sb.append(strEvent);
//            added++;
//         }
//      }
//
//      sb.append("            break;\n");
//      //   FOCUS_OUT
//      sb.append("\n         case ControlEvent.FOCUS_OUT:\n");
//
//      added = 0;
//      for (i = 0; i < itemCount; i++)
//      {
//         String strEvent = items[i].getJavaEventFOCUS_OUT();
//         if (strEvent.length() > 0)
//         {
//            if (added > 0)
//               sb.append("            else\n");
//            sb.append(strEvent);
//            added++;
//         }
//      }
//
//      sb.append("            break;\n");
//      sb.append("      }\n");
//      sb.append("   }\n\n");
//
//      //   EXTRA METHODS
//      sb.append("   // Called before the popup starts\n   protected void onPopup()\n   {\n   }\n\n");
//      sb.append("   // Called after the popup is finished\n   protected void postPopup()\n   {\n   }\n\n");
//      sb.append("   // Called before the unpop starts\n   protected void onUnpop()\n   {\n   }\n\n");
//      sb.append("   // Called after the unpop is finished\n   protected void postUnpop()\n   {\n   }\n\n");
//
//      // FINISH
//      sb.append("}\n");
//
//      return sb.toString();
//   }
	
//   private String prepareJavaAsContainer()
//   {
//      int    i;
//      StringBuffer sb = new StringBuffer(2048); // StringBuffer in this case is far more efficient. (code size dropped 500 bytes)
//
//      // imports
//      sb.append("// ").append(projectName).append(".java\n\n");
//
//      sb.append("import waba.ui.*;\n");
//      sb.append("import waba.fx.*;\n");
//      sb.append("import waba.sys.*;\n\n");
//
//      // class declaration
//      sb.append("/**\n");
//      sb.append(" * Class declaration\n");
//      sb.append(" *\n");
//      sb.append(" * @author\n");
//      sb.append(" */\n\n");
//
//      sb.append("public class ").append(projectName).append(" extends Container\n{\n");
//      sb.append("   // declarations\n");
//
//      // variables
//      for (i = 0; i < itemCount; i++)
//         sb.append(items[i].getJavaDeclaration());
//
//      // constructor
//      sb.append("\n\n   // init\n   public ").append(projectName).append("(Rect bounds)\n   {\n");
//      sb.append("      // a Container must have its bounds set before any control is added\n");
//      sb.append("      setRect(bounds);\n");
//      sb.append("      if (Settings.isColor)\n");
//      sb.append("         setBackForeColors(Color.getColor(0x").append(getBackColor()).append("), Color.getColor(0x").append(getForeColor()).append("));\n\n");
//
//      for (i = 0; i < itemCount; i++)
//         sb.append(items[i].getJavaInit());
//
//      sb.append("\n      if (Settings.isColor)\n      {\n");
//
//      for (i = 0; i < itemCount; i++)
//         sb.append("         ").append(items[i].getTitle()).append(".setBackForeColors(Color.getColor(0x").append(items[i].getBack()).append("), Color.getColor(0x").append(items[i].getFore()).append("));\n");
//
//      sb.append("\n      }\n   }\n\n");
//
//      // onEvent
//      sb.append("   public void onEvent(Event event)\n");
//      sb.append("   {\n");
//      sb.append("      switch (event.type)\n");
//      sb.append("      {\n");
//      //   PRESSED
//      sb.append("         case ControlEvent.PRESSED:\n");
//
//      int added = 0;
//      for (i = 0; i < itemCount; i++)
//      {
//         String strEvent = items[i].getJavaEventPRESSED();
//         if (strEvent.length() > 0)
//         {
//            if (added > 0)
//               sb.append("            else\n");
//            sb.append(strEvent);
//            added++;
//         }
//      }
//
//      sb.append("            break;\n");
//      //   FOCUS_OUT
//      sb.append("\n         case ControlEvent.FOCUS_OUT:\n");
//
//      added = 0;
//      for (i = 0; i < itemCount; i++)
//      {
//         String strEvent = items[i].getJavaEventFOCUS_OUT();
//         if (strEvent.length() > 0)
//         {
//            if (added > 0)
//               sb.append("            else\n");
//            sb.append(strEvent);
//            added++;
//         }
//      }
//
//      sb.append("            break;\n");
//      sb.append("      }\n");
//      sb.append("   }\n");
//
//      // FINISH
//      sb.append("}\n");
//
//      return sb.toString();
//   }
	
	private String prepareData() {
		StringBuffer sb = new StringBuffer(1024);

		sb.append(Persistant.MEMO_TITLE).append(projectName).append('\n');
		sb.append(version).append('\n');
		int type = 0;// miAsMainWindow.isChecked ? 0 : miAsWindow.isChecked ? 1 : 2;
		sb.append(type).append('\n');
		// Window?
		if (type == 1) {
			sb.append(wcWidth).append('\n');
			sb.append(wcHeight).append('\n');
		}
		sb.append(getForeColor()).append('\n');
		sb.append(getBackColor()).append('\n');
		for (Item i : items) {
			sb.append(i.getAllInfo());
		}

		return sb.toString();
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
		updateTitle();
	}
}

