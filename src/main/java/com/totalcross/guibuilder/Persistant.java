package com.totalcross.guibuilder;

import totalcross.io.DataStream;
import totalcross.io.FileNotFoundException;
import totalcross.io.IOException;
import totalcross.io.IllegalArgumentIOException;
import totalcross.io.PDBFile;
import totalcross.io.ResizeRecord;
import totalcross.sys.Vm;
import totalcross.ui.ComboBox;
import totalcross.util.Vector;

/**
 * Persistant - save text data in memo pad or in catalog
 *
 *
 * @author Yehuda Miron ver 1.0 - image only
 * @author Yehuda Miron ver 2.0 - string too
 * @author Yehuda Miron ver 3.0 - list catalog
 * @author Yehuda Miron ver 4.0 - clean up and documentation
 * @author Guich ver 5.0 - more cleanup
 * @version 5.0
 */
public class Persistant
{
   public static final String MEMO_TITLE = "//GuiBuilder  ";
   private final String CREATOR_ID = "YMym"; // creator ids can't be all lowercase
   private String typeC = "NONE";

   private String textBody;
   private int    indexInText;
   private int    textSize;

   /**
    * Constructor declaration
    * @param prefix
    */
   public Persistant(String prefix)
   {
      typeC = prefix;
   }

   /**
    * createCatalogName - add creator id & typec
    * as needed in palm OS standard
    *
    *
    * @param fname - catalog name without prefix & sufix
    *
    * @return catalog name including prefix & suffix
    */
   private String createCatalogName(String fname)
   {
      String    catalogName;

      catalogName = fname + '.' + CREATOR_ID + '.' + typeC;

      return catalogName;

   }

   /**
    * saveString - save a string in a catalog or memo.
    * this function get the whole body of
    * catalog as a single string
    *
    *
    * @param fName - catalog name without prefix & suffix
    * @param useMemo - true for save in memo, false for catalog
    * @param body - a single string (multi line if needed)
    * that is the whole body of the catalog.
    *
    * @return true - if successful
    * false - otherwise
    */
   public boolean saveString(String fName, boolean useMemo, String body)
	{
		PDBFile cat;
		ResizeRecord rs;
		DataStream ds;
		String catalogName;
		try {
			catalogName = createCatalogName(fName);
			cat = new PDBFile(catalogName, PDBFile.CREATE_EMPTY);

			rs = new ResizeRecord(cat, 512);
			ds = new DataStream(rs);

			rs.startRecord();
			ds.writeString(body);
			rs.endRecord();

			cat.close();
		} catch (IOException e) {
		}
		return true;
	}


   /**
    * loadString - load the body of the catalog into a single string
    * (multi line if needed)
    *
    *
    * @param fName - name of catalog without prefix & suffix
    *
    * @return string - body of catalog
    * null   - in case of error in read
    *
    * @see
    */
   private String loadStringFromCat(String fName)
   {
      String            body=null;
      PDBFile           cat;
      ResizeRecord      rs;
      DataStream        ds;
      String            catalogName;


      catalogName = createCatalogName(fName);

		try {
			cat = new PDBFile(catalogName, PDBFile.READ_WRITE);
			// create the streams

			rs = new ResizeRecord(cat, 512);
			ds = new DataStream(rs);

			try {
				cat.setRecordPos(0);
				body = ds.readString();
			} catch (IOException e) {
			}

			cat.close();
		} catch (IllegalArgumentIOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

      

      return body;
   }

   /**
    * findText - load text from catalog and save it in
    * private buffer for retrieving it later on
    *
    * @param title name of catalog to read (without prefix & suffix)
    * @param isMemo true for memo, false for catalog
    *
    * @return true if text found, false otherwise
    */
   public boolean findText(String title, boolean isMemo)
   {
     String s = loadStringFromCat(title);

     if (s != null)
     {
        textBody = s;
        textSize = textBody.length();
        indexInText = 0;
        s = getTextLine();

        return true;
     }

      return false;
   }


   /**
    * getNextLine - read next line from private buffer
    *
    *
    * @return single line
    *
    * @see #findText(String, boolean)
    */
   public String getTextLine()
   {
      String    s1;
      int       i;

      if (indexInText > textSize - 1)
         return null;

      i = textBody.indexOf('\n', indexInText);

      if (-1 == i)      // not found return till end of text
         i = textSize;

      s1 = textBody.substring(indexInText, i);
      indexInText = i + 1;

      return s1;
   }



   /**
    * Method read palm catalog
    * and put the relevant items in the combobox
    * (only items with correct group_id & typeC are added)
    *
    * @param cb ComboBox to fill with data
    */
   public void fillListNames(ComboBox cb, boolean useMemo)
   {
      // clear ComboBox
      cb.removeAll();
      Vector v = new Vector(20);
      String fullName;

      {
         String []files = PDBFile.listPDBs(Integer.valueOf(CREATOR_ID),Integer.valueOf(typeC));
         if (files != null)
         {
            int n = files.length;
            for (int i =0; i < n; i++)
            {
               fullName = files[i];
               if (fullName != null) // note: resource files are returned as null
                  v.addElement(fullName.substring(0, fullName.length()-10));
            }
         }
      }

      if (v.size() > 0)
      {
         cb.add((String[])v.toObjectArray()); // note: since we added strings, we can securely do the cast.
         cb.setSelectedIndex(0);
      }
   }
   /** Remove the desired file from memo/catalog */
   public boolean remove(String fileName, boolean useMemo)
   {
		try {
			String catalogName = createCatalogName(fileName);
			PDBFile cat = new PDBFile(catalogName, PDBFile.READ_WRITE);
			cat.delete();
			return true;
		} catch (IOException e) {
		}
      return false;
   }
}

