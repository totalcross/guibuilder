package com.totalcross.guibuilder.tabs;

import java.util.List;
import java.util.function.Supplier;

import com.totalcross.guibuilder.ItemAttributes;
import com.totalcross.guibuilder.Validation;
import com.totalcross.guibuilder.items.Item;

import totalcross.ui.ColorList;
import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import totalcross.ui.Edit;
import totalcross.ui.Label;

public class Attributes extends Container {
	private Edit nameEdit;
	private Edit textEdit;
	private ComboBox foreCombo;
	private ComboBox backCombo;

	@Override
	public void initUI() {
		super.initUI();
		
		Label nameLb, textLb, forecolorLb, backcolorLb;

		add(nameLb = new Label("Name sufix:"), LEFT, TOP);
		add(textLb = new Label("Text:"), SAME, AFTER);
		add(forecolorLb = new Label("Fore color:"), SAME, AFTER);
		add(backcolorLb = new Label("Back color:"), SAME, AFTER);
		
		
		add(nameEdit = new Edit(), AFTER, SAME, FILL, SAME, nameLb);
		add(textEdit = new Edit(), AFTER, SAME, FILL, SAME, textLb);
		add(foreCombo = new ComboBox(new ColorList()), AFTER, SAME, FILL, SAME, forecolorLb);
		add(backCombo = new ComboBox(new ColorList()), AFTER, SAME, FILL, SAME, backcolorLb);

		foreCombo.setSelectedItem("000000");
		backCombo.setSelectedItem("FFFFFF");
		
		nameEdit.setText("Name1");
	}

	public void populateFromScreen(ItemAttributes itemAttr) {
		itemAttr.setName(nameEdit.getText());
		itemAttr.setText(textEdit.getText());
		itemAttr.setFore(foreCombo.getSelectedItem().toString());
		itemAttr.setBack(backCombo.getSelectedItem().toString());
	}

	public void clearProject() {
		textEdit.setText("");
		nameEdit.setText("Name1");
	}
	
}
