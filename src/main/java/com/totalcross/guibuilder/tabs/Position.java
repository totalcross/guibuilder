package com.totalcross.guibuilder.tabs;

import java.util.HashMap;
import java.util.List;

import com.totalcross.guibuilder.ItemAttributes;
import com.totalcross.guibuilder.items.Item;

import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import totalcross.ui.Edit;
import totalcross.ui.Label;

public class Position extends Container {
	Edit syEdit;
	Edit yEdit;
	Edit sxEdit;
	Edit xEdit;
	ComboBox xCombo;
	ComboBox yCombo;
	ComboBox sxCombo;
	ComboBox syCombo;
	ComboBox relCombo;
	
	private String getBaseRelativeValue() {
		return "Last ctrl added";
	}

	@Override
	public void initUI() {
		super.initUI();

		// now the position delta edits
		add(xEdit = new Edit("99"), RIGHT, TOP + 1);
		add(yEdit = new Edit("99"), RIGHT, AFTER);
		add(sxEdit = new Edit("99"), RIGHT, AFTER);
		add(syEdit = new Edit("99"), RIGHT, AFTER);

		// ...their relative coords
		add(xCombo = new ComboBox(new String[] { "LEFT", "CENTER", "RIGHT", "BEFORE", "AFTER", "SAME", "PREFERRED" }), BEFORE - 4, SAME, xEdit);
		add(yCombo = new ComboBox(new String[] { "TOP", "CENTER", "BOTTOM", "BEFORE", "AFTER", "SAME", "PREFERRED" }), BEFORE - 4, SAME, yEdit);
		add(sxCombo = new ComboBox(new String[] { "PREFERRED", "SAME", "FILL", "FIT" }), BEFORE - 4, SAME, sxEdit);
		add(syCombo = new ComboBox(new String[] { "PREFERRED", "SAME", "FILL", "FIT" }), BEFORE - 4, SAME, syEdit);
		add(relCombo = new ComboBox(new String[] { getBaseRelativeValue() }), SAME, AFTER, FILL, PREFERRED); // same  width  of  the  pbgControls
		// ...and their captions
		add(new Label("x+delta:"), BEFORE - 4, SAME + 2, xCombo);
		add(new Label("y+delta:"), BEFORE - 4, SAME + 2, yCombo);
		add(new Label("sx+delta:"), BEFORE - 4, SAME + 2, sxCombo);
		add(new Label("sy+delta:"), BEFORE - 4, SAME + 2, syCombo);
		add(new Label("Relative to:"), BEFORE - 4, SAME + 1, relCombo);

		// set other parameters for the controls
		String numbers = "-0123456789";
		xEdit.setValidChars(numbers);
		xEdit.setMaxLength(3);
		yEdit.setValidChars(numbers);
		yEdit.setMaxLength(3);
		sxEdit.setValidChars(numbers);
		sxEdit.setMaxLength(3);
		syEdit.setValidChars(numbers);
		syEdit.setMaxLength(3);
		xCombo.remove(6); // was used just to set the PREFERRED size
		yCombo.remove(6);
		xCombo.setSelectedIndex(0);
		yCombo.setSelectedIndex(0);
		sxCombo.setSelectedIndex(0);
		syCombo.setSelectedIndex(0);
		relCombo.setSelectedIndex(0);

		xEdit.setText("0");
		yEdit.setText("0");
		sxEdit.setText("0");
		syEdit.setText("0");

//		{
//			int c1 = controls.getBackColor(); // trick: to set the color that we
//												// want, we must make it darker
//												// bc the control will make it
//												// brighter. This is needed
//												// because the grayscale
//												// support.
//			xCombo.setBackColor(c1);
//			yCombo.setBackColor(c1);
//			sxCombo.setBackColor(c1);
//			syCombo.setBackColor(c1);
//			relCombo.setBackColor(c1);
//			// foreCombo.setBackColor(c1);
//			// backCombo.setBackColor(c1);
//			itemsList.setBackColor(Color.darker(Color.getRGB(0x33, 0x99, 0xFF)));
//			pbgActions.setBackColor(itemsList.getBackColor());
//			pbgActions.setCursorColor(Color.getRGB(0, 0, 0xCC));
//			// change the color of the menu
//			mbar.setBackForeColors(Color.getRGB(0, 0, 255), Color.WHITE);
//			mbar.setCursorColor(Color.getRGB(100, 100, 255));
//			mbar.setBorderStyle(NO_BORDER);
//			// mbar.setPopColors(Color.getRGB(0,120,255),Color.getRGB(0,255,255),null);
//			// // use the default cursor color for the popup menu (last null
//			// param)
//		}
	}

	public void clearProject() {
		clearRelatives();
	}
	
	private void clearRelatives() {
		relCombo.removeAll();
		relCombo.add(getBaseRelativeValue());
		relCombo.setSelectedIndex(0);
	}
	
	public static final HashMap<String, Integer> htConst2Val = new HashMap<>();
	
	static {
		htConst2Val.put("LEFT", LEFT);
		htConst2Val.put("CENTER", CENTER);
		htConst2Val.put("RIGHT", RIGHT);
		htConst2Val.put("BEFORE", BEFORE);
		htConst2Val.put("AFTER", AFTER);
		htConst2Val.put("SAME", SAME);
		htConst2Val.put("TOP", TOP);
		htConst2Val.put("BOTTOM", BOTTOM);
		htConst2Val.put("PREFERRED", PREFERRED);
		htConst2Val.put("SAME", SAME);
		htConst2Val.put("FILL", FILL);
		htConst2Val.put("FIT", FIT);
	}

	public void populateFromScreen(ItemAttributes itemAttr) {
		int xx = htConst2Val.get(xCombo.getSelectedItem()) + Integer.valueOf(xEdit.getText());
		int yy = htConst2Val.get(yCombo.getSelectedItem()) + Integer.valueOf(yEdit.getText());
		int sx = htConst2Val.get(sxCombo.getSelectedItem()) + Integer.valueOf(sxEdit.getText());
		int sy = htConst2Val.get(syCombo.getSelectedItem()) + Integer.valueOf(syEdit.getText());
		
		itemAttr.setX(xx);
		itemAttr.setY(yy);
		itemAttr.setSx(sx);
		itemAttr.setSy(sy);
		itemAttr.setRelTo(relCombo.getSelectedIndex() != 0? relCombo.getSelectedItem().toString(): "");
	}

	public void resetRelatives(List<Item> items) {
		clearRelatives();
		
		for (Item i: items) {
			relCombo.add(i.getTitle());
		}
		
		relCombo.setSelectedIndex(0);
	}
	
}
