package com.totalcross.guibuilder;

import com.totalcross.guibuilder.items.Item;

public class ItemAttributes {
	private String type;
	private String name;
	private String text;
	private String fore;
	private String back;
	private int x;
	private int y;
	private int sx;
	private int sy;
	private String relTo;
	
	public String getTitle() {
		return getPrefix() + getName();
	}
	
	private String getPrefix() {
		return Item.getPrefix(type);
	}

	public ItemAttributes() {
	}
	
	public ItemAttributes confType(String type) {
		setType(type);
		return this;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFore() {
		return fore;
	}

	public void setFore(String fore) {
		this.fore = fore;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSx() {
		return sx;
	}

	public void setSx(int sx) {
		this.sx = sx;
	}

	public int getSy() {
		return sy;
	}

	public void setSy(int sy) {
		this.sy = sy;
	}

	public String getRelTo() {
		return relTo;
	}

	public void setRelTo(String relTo) {
		this.relTo = relTo;
	}
	
	
}
