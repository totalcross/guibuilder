package com.totalcross.guibuilder.ui.pbg;

import com.totalcross.guibuilder.ui.ActionPair;

public class NameAction extends ActionPair<String> {

	public NameAction(String name, Runnable action) {
		super(name, action);
	}

	public NameAction(String name) {
		this(name, null);
	}
	
	public NameAction() {
		this(null);
	}

}
