package com.totalcross.guibuilder.ui.pbg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.totalcross.guibuilder.ui.UtilUI;

import totalcross.ui.PushButtonGroup;

public class PBGBuilder extends ArrayList<NameAction> {
	private static final long serialVersionUID = 5246336174594325682L;
	
	@Override
	public boolean add(NameAction e) {
		return super.add(e);
	}
	
	public boolean add(String name) {
		return add(new NameAction(name));
	}
	
	public boolean addNull() {
		return add(new NameAction());
	}
	
	public PushButtonGroup build(boolean atLeastOne, int selected, int gap, int insideGap, int rows, boolean allSameWidth, byte type) {
		String[] names = new String[size()];
		ArrayList<Runnable> actionMap = new ArrayList<>();
		
		int idx = 0;
		for (NameAction nameAction: this) {
			names[idx] = nameAction.obj;
			actionMap.add(nameAction.action);
			
			idx++;
		}
		
		PushButtonGroup pbg = new PushButtonGroup(names, atLeastOne, selected, gap, insideGap, rows, allSameWidth, type);
		
		pbg.addPressListener(UtilUI.getMapPressListener(pbg::getSelectedIndex, (i) -> {
			if (i >= 0 && i < actionMap.size()) {
				return actionMap.get(i);
			} else {
				return null;
			}
		}));
		
		return pbg;
	}
}
