package com.totalcross.guibuilder.ui;

import java.util.function.IntFunction;
import java.util.function.IntSupplier;

import totalcross.ui.event.PressListener;

public class UtilUI {
	// utility class deserve only static methods
	private UtilUI() {}

	public static PressListener getMapPressListener(IntSupplier supplier, IntFunction<Runnable> actionMap) {
		return (e) -> {
			Runnable action = actionMap.apply(supplier.getAsInt());

			if (action != null) {
				action.run();
			}
		};
	}
}
