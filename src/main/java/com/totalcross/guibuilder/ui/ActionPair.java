package com.totalcross.guibuilder.ui;

public class ActionPair<T> {
	public final T obj;
	public final Runnable action;
	
	public ActionPair(T obj, Runnable action) {
		this.obj = obj;
		this.action = action;
	}
}
