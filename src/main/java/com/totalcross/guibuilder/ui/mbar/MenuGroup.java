package com.totalcross.guibuilder.ui.mbar;

import java.util.ArrayList;

public class MenuGroup extends ArrayList<MenuItemAction> {
	private static final long serialVersionUID = 828960542403037966L;
	
	public final String groupTitle;

	public MenuGroup(String groupTitle) {
		this.groupTitle = groupTitle;
	}
}
