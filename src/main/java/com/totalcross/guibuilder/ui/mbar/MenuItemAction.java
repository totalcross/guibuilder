package com.totalcross.guibuilder.ui.mbar;

import com.totalcross.guibuilder.ui.ActionPair;

import totalcross.ui.MenuItem;

public class MenuItemAction extends ActionPair<MenuItem> {
	public static MenuItemAction getItemSeparator() {
		return new MenuItemAction();
	}
	
	public MenuItemAction(MenuItem mi, Runnable action) {
		super(mi, action);
	}
	
	public MenuItemAction(String text, Runnable action) {
		this(new MenuItem(text), action);
	}


	public MenuItemAction(MenuItem mi) {
		this(mi, null);
	}
	
	public MenuItemAction(String text) {
		this(new MenuItem(text));
	}
	
	public MenuItemAction() {
		this(new MenuItem());
	}
}
