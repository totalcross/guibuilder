package com.totalcross.guibuilder.ui.mbar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import totalcross.ui.MenuItem;

public class MenuGroupsActions {
	private Map<Integer, Runnable> actionMap;
	private MenuItem[][] items;
	
	public MenuGroupsActions(List<MenuGroup> groups) {
		actionMap = new HashMap<>();
		items = new MenuItem[groups.size()][];
		
		int groupIdx = 0;
		for (MenuGroup group: groups) {
			MenuItem[] itemGroup = new MenuItem[group.size() + 1];
			
			itemGroup[0] = new MenuItem(group.groupTitle);
			
			int itemIdx = 1;
			
			for (MenuItemAction itemAction: group) {
				actionMap.put(itemIdx + 100*groupIdx, itemAction.action);
				itemGroup[itemIdx] = itemAction.obj;
				itemIdx++;
			}
			
			items[groupIdx] = itemGroup;
			groupIdx++;
		}
	}
	
	public Map<Integer, Runnable> getActionMap() {
		return actionMap;
	}

	public MenuItem[][] getItems() {
		return items;
	}

}
