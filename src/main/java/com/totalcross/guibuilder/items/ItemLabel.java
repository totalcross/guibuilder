package com.totalcross.guibuilder.items;

import totalcross.ui.Label;

public class ItemLabel extends Item {
	@Override
	public String getPrefix() {
		return "lab";
	}

	@Override
	public String getClassName() {
		return "Label";
	}

	// no PRESSED events generated
	@Override
	public String getJavaEventPRESSED() {
		return "";
	}

	@Override
	protected Label getControl() {
		return new Label(text);
	}
}
