package com.totalcross.guibuilder.items;

import totalcross.ui.Button;

public class ItemButton extends Item {
	@Override
	public String getPrefix() {
		return "btn";
	}
	
	@Override
	public String getClassName() {
		return "Button";
	}
	
	@Override
	protected Button getControl() {
		return new Button(text);
	}
}
