package com.totalcross.guibuilder.items;

import totalcross.sys.Convert;
import totalcross.ui.ListBox;

public class ItemListBox extends Item {
	String[] items;

	@Override
	public String getPrefix() {
		return "lbx";
	}

	@Override
	public String getClassName() {
		return "ListBox";
	}

	@Override
	public void update(String name, String text, String fore, String back, int x, int y, int sx, int sy, String relTo) {
		super.update(name, text, fore, back, x, y, sx, sy, relTo);
		items = Convert.tokenizeString(text, ',');
	}

	@Override
	protected String getConstructorParams() {
		if (items == null || items.length == 0 || items[0].length() == 0) {
			return "";
		}

		StringBuffer sb = new StringBuffer(128);
		sb.append("new String []{");
		for (int i = 0; i < items.length; i++) {
			if (i > 0) {
				sb.append(',');
			}
			sb.append('"');
			sb.append(items[i]);
			sb.append('"');
		}
		sb.append('}');
		return sb.toString();
	}

	@Override
	protected ListBox getControl() {
		return new ListBox(items);
	}
}
