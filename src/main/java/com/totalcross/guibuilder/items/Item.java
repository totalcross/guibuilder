package com.totalcross.guibuilder.items;

import java.util.HashMap;
import java.util.Map;

import totalcross.ui.Control;
import totalcross.ui.gfx.Color;

public abstract class Item {
	public static Map<String, Item> htItems = new HashMap<>();
	
	private static Map<String, Control> bufferedControls = new HashMap<>();
	
	private static Map<String, Class<Item>> type2class = new HashMap<>();
	private static Map<String, Item> type2sample = new HashMap<>();
	protected int x, y, sx, sy;
	protected String name;
	protected String relTo;
	protected String text;
	protected String back;
	protected String fore;
	
	public static Class<Item> getSubclass(String type) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		if (type2class.get(type) != null) {
			return type2class.get(type);
		}
		@SuppressWarnings("unchecked")
		Class<Item> c = (Class<Item>) Class.forName("com.totalcross.guibuilder.items.Item" + type); // E.G.: ItemButton
		
		inputClassBuffer(type, c);
		
		return c;
	}
	
	private static void inputClassBuffer(String type, Class<Item> c) throws InstantiationException, IllegalAccessException {
		type2class.put(type, c);
		type2sample.put(type, c.newInstance());
	}
	
	private static Item getSample(String type) {
		if (type2sample.get(type) == null) {
			try {
				getSubclass(type); // this also populates sample
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			}
		}
		return type2sample.get(type);
	}

	public static String getPrefix(String type) {
		Item sample = getSample(type);
		return sample != null? sample.getPrefix(): null;
	}

	public void update(String name, String text, String fore, String back, int x, int y, int sx, int sy, String relTo) {
		this.name = name;
		this.text = text;
		this.fore = fore;
		this.back = back;
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.relTo = relTo;
	}

	public void updatePosition(Control control) {
		control.setRect(x, y, sx, sy, Item.safeRelatedControl(relTo));
	}
	
	public static void resetControls() {
		bufferedControls.clear();
	}

	public static Control safeRelatedControl(String itemKey) {
		Control bufferedControl = bufferedControls.get(itemKey);
		
		if (bufferedControl != null) {
			return bufferedControl;
		}
		Item i = htItems.get(itemKey);
		if (i != null) {
			bufferedControl = i.getControlInitialized();
			bufferedControls.put(itemKey, bufferedControl);
		}
		
		return bufferedControl;
	}

	public Control getControlInitialized() {
		Control control = getControl();
		control.setBackForeColors(Color.getRGB(back), Color.getRGB(fore));
		return control;
	}

	public abstract String getPrefix();

	public abstract String getClassName();

	public String getTitle() {
		return getPrefix() + name;
	}

	public String getJavaDeclaration() {
		return new StringBuffer(128).append("   ").append(getClassName()).append(' ').append(getTitle()).append(";\n")
				.toString();
	}

	public String[] getRelativeX() {
		String sel = "";
		int delta = 0;
		if ((Control.LEFT - Control.RANGE) <= x && x <= (Control.LEFT + Control.RANGE)) {
			sel = "LEFT";
			delta = x - Control.LEFT;
		} else if ((Control.CENTER - Control.RANGE) <= x && x <= (Control.CENTER + Control.RANGE)) {
			sel = "CENTER";
			delta = x - Control.CENTER;
		} else if ((Control.RIGHT - Control.RANGE) <= x && x <= (Control.RIGHT + Control.RANGE)) {
			sel = "RIGHT";
			delta = x - Control.RIGHT;
		} else if ((Control.BEFORE - Control.RANGE) <= x && x <= (Control.BEFORE + Control.RANGE)) {
			sel = "BEFORE";
			delta = x - Control.BEFORE;
		} else if ((Control.AFTER - Control.RANGE) <= x && x <= (Control.AFTER + Control.RANGE)) {
			sel = "AFTER";
			delta = x - Control.AFTER;
		} else if ((Control.SAME - Control.RANGE) <= x && x <= (Control.SAME + Control.RANGE)) {
			sel = "SAME";
			delta = x - Control.SAME;
		}
		return new String[] { sel, delta + "" };
	}

	public String[] getRelativeY() {
		String sel = "";
		int delta = 0;
		if ((Control.TOP - Control.RANGE) <= y && y <= (Control.TOP + Control.RANGE)) {
			sel = "TOP";
			delta = y - Control.TOP;
		} else if ((Control.CENTER - Control.RANGE) <= y && y <= (Control.CENTER + Control.RANGE)) {
			sel = "CENTER";
			delta = y - Control.CENTER;
		} else if ((Control.BOTTOM - Control.RANGE) <= y && y <= (Control.BOTTOM + Control.RANGE)) {
			sel = "BOTTOM";
			delta = y - Control.BOTTOM;
		} else if ((Control.BEFORE - Control.RANGE) <= y && y <= (Control.BEFORE + Control.RANGE)) {
			sel = "BEFORE";
			delta = y - Control.BEFORE;
		} else if ((Control.AFTER - Control.RANGE) <= y && y <= (Control.AFTER + Control.RANGE)) {
			sel = "AFTER";
			delta = y - Control.AFTER;
		} else if ((Control.SAME - Control.RANGE) <= y && y <= (Control.SAME + Control.RANGE)) {
			sel = "SAME";
			delta = y - Control.SAME;
		}
		return new String[] { sel, delta + "" };
	}

	public String[] getRelativeSX() {
		String sel = "";
		int delta = 0;
		if ((Control.PREFERRED - Control.RANGE) <= sx && sx <= (Control.PREFERRED + Control.RANGE)) {
			sel = "PREFERRED";
			delta = sx - Control.PREFERRED;
		} else if ((Control.SAME - Control.RANGE) <= sx && sx <= (Control.SAME + Control.RANGE)) {
			sel = "SAME";
			delta = sx - Control.SAME;
		} else if ((Control.FILL - Control.RANGE) <= sx && sx <= (Control.FILL + Control.RANGE)) {
			sel = "FILL";
			delta = sx - Control.FILL;
		}
		if ((Control.FIT - Control.RANGE) <= sx && sx <= (Control.FIT + Control.RANGE)) {
			sel = "FIT";
			delta = sx - Control.FIT;
		}
		return new String[] { sel, delta + "" };
	}

	public String[] getRelativeSY() {
		String sel = "";
		int delta = 0;
		if ((Control.PREFERRED - Control.RANGE) <= sy && sy <= (Control.PREFERRED + Control.RANGE)) {
			sel = "PREFERRED";
			delta = sy - Control.PREFERRED;
		} else if ((Control.SAME - Control.RANGE) <= sy && sy <= (Control.SAME + Control.RANGE)) {
			sel = "SAME";
			delta = sy - Control.SAME;
		} else if ((Control.FILL - Control.RANGE) <= sy && sy <= (Control.FILL + Control.RANGE)) {
			sel = "FILL";
			delta = sy - Control.FILL;
		}
		if ((Control.FIT - Control.RANGE) <= sy && sy <= (Control.FIT + Control.RANGE)) {
			sel = "FIT";
			delta = sy - Control.FIT;
		}
		return new String[] { sel, delta + "" };
	}

	public void setRelativeTo(String relTo) {
		this.relTo = relTo;
	}

	public String getJavaInit() {
		StringBuffer sb = new StringBuffer(256);
		String title = getTitle();
		String[] rx = getRelativeX();
		String[] ry = getRelativeY();
		String[] rsx = getRelativeSX();
		String[] rsy = getRelativeSY();
		boolean isPreferredSX = rsx[0].equals("PREFERRED");
		boolean isPreferredSY = rsy[0].equals("PREFERRED");
		int deltaX = Integer.valueOf(rx[1]);
		int deltaY = Integer.valueOf(ry[1]);
		int deltaSX = Integer.valueOf(rsx[1]);
		int deltaSY = Integer.valueOf(rsy[1]);
		boolean hasRelTo = relTo.length() > 0;

		sb.append("      add(").append(title).append(" = new ").append(getClassName()).append('(').append(getConstructorParams()).append(')');
		// now we need to decide the best type of construction
		
		if (isPreferredSX && isPreferredSY && deltaSX == 0 && deltaSY == 0)  {
			// add(new Control(), x, y)
			
			sb.append(", ");
			sb.append(rx[0]);
			if (deltaX != 0) {
				if (deltaX > 0) {
					sb.append('+');
				}
				sb.append(rx[1]);
			}
			sb.append(", ");
			sb.append(ry[0]);
			if (deltaY != 0) {
				if (deltaY > 0) {
					sb.append('+');
				}
				sb.append(ry[1]);
			}
			if (hasRelTo) {
				sb.append(", ").append(relTo);
			}
			sb.append(");\n");
		} else {
			// add(new Control()); control.setRect(...);
			
			sb.append(");\n");
			sb.append("      ").append(title).append(".setRect(");
			sb.append(rx[0]);
			if (deltaX != 0) {
				if (deltaX > 0) {
					sb.append('+');
				}
				sb.append(rx[1]);
			}
			sb.append(", ");
			sb.append(ry[0]);
			if (deltaY != 0) {
				if (deltaY > 0) {
					sb.append('+');
				}
				sb.append(ry[1]);
			}
			sb.append(", ");
			sb.append(rsx[0]);
			if (deltaSX != 0) {
				if (deltaSX > 0) {
					sb.append('+');
				}
				sb.append(rsx[1]);
			}
			sb.append(", ");
			sb.append(rsy[0]);
			if (deltaSY != 0) {
				if (deltaSY > 0) {
					sb.append('+');
				}
				sb.append(rsy[1]);
			}
			if (hasRelTo) {
				sb.append(", ").append(relTo);
			}
			sb.append(");\n");
		}

		return sb.toString();
	}

	protected String getConstructorParams() {
		return '"' + text + '"';
	}

	public String getJavaEventPRESSED() {
		return "            if (event.target == " + getTitle() + ")\n" + "            {\n            }\n";
	}

	public String getJavaEventFOCUS_OUT() {
		return "";
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getSX() {
		return sx;
	}

	public int getSY() {
		return sy;
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}

	public String getFore() {
		return fore;
	}

	public String getBack() {
		return back;
	}

	protected abstract Control getControl();

	public String relativeTo() {
		return relTo;
	}

	public String getAllInfo() {
		return getClassName() + '\n' + name + '\n' + text + '\n' + fore + '\n' + back + '\n' + x + '\n' + y + '\n' + sx
				+ '\n' + sy + '\n' + relTo + '\n';
	}

	@Override
	public String toString() {
		return getTitle();
	}
}
