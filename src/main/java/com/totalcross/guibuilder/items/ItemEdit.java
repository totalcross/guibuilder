package com.totalcross.guibuilder.items;

import totalcross.ui.Edit;

public class ItemEdit extends Item {
	@Override
	public String getPrefix() {
		return "edt";
	}

	@Override
	public String getClassName() {
		return "Edit";
	}

	// no PRESSED events generated
	@Override
	public String getJavaEventPRESSED() {
		return "";
	}

	@Override
	public String getJavaEventFOCUS_OUT() {
		return "            if (event.target == " + getTitle() + ")\n" + "            {\n            }\n";
	}

	@Override
	protected Edit getControl() {
		return new Edit(text);
	}
}
