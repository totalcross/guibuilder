package com.totalcross.guibuilder.items;

import totalcross.ui.Check;

public class ItemCheck extends Item {
	@Override
	public String getPrefix() {
		return "chk";
	}

	@Override
	public String getClassName() {
		return "Check";
	}

	@Override
	protected Check getControl() {
		return new Check(text);
	}
}
