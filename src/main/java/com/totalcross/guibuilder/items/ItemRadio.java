package com.totalcross.guibuilder.items;

import totalcross.ui.Radio;

public class ItemRadio extends Item {
	@Override
	public String getPrefix() {
		return "rad";
	}

	@Override
	public String getClassName() {
		return "Radio";
	}

	@Override
	protected Radio getControl() {
		return new Radio(text);
	}
	
}
