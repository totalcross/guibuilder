package com.totalcross.guibuilder;

import java.util.ArrayList;
import java.util.List;

public class Validation {
	public final String note;
	public final boolean error;
	
	public Validation(String note, boolean isError) {
		this.error = isError;
		this.note = note;
	}
	
	public static boolean hasNotes(List<Validation> l) {
		return l.size() > 0;
	}
	
	public static boolean hasErrors(List<Validation> l) {
		for (Validation v: l) {
			if (v.error) {
				return true;
			}
		}
		return false;
	}
	
	public static List<Validation> getErrors(List<Validation> l) {
		List<Validation> e = new ArrayList<>();
		for (Validation v: l) {
			if (v.error) {
				e.add(v);
			}
		}
		return e;
	}
}
